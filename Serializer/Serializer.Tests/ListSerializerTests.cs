﻿using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Serializer.Tests
{
    public class ListSerializerTests
    {
        private IListSerializer _listSerializer;

        [SetUp]
        public void Setup()
        {
            _listSerializer = new ListSerializer();
        }

        [Test]
        [TestCaseSource(nameof(TestCases))]
        public async Task Positive(ListNode head)
        {
            var deepCopy = await _listSerializer.DeepCopy(head);
            Assert.AreEqual(JsonSerializer.Serialize(head), JsonSerializer.Serialize(deepCopy));
            Assert.IsFalse(ReferenceEquals(head, deepCopy));
        }

        [Test]
        public async Task CheckNull()
        {
            var deepCopy = await _listSerializer.DeepCopy(null);
            Assert.IsNull(deepCopy);
        }

        private static IEnumerable<ListNode> TestCases()
        {
            var head = new ListNode
            {
                Data = "1"
            };

            yield return head;

            var secondNode = new ListNode
            {
                Data = "2124з90шыьтвалдыптызпш3-0еш623ьтжьslgkd;fgmndksvmdsjkv0o53[6k'5m46m'mg;dfmv;lms;v,sb';,m'sdmb[054i6[j;sng;ndklsgnkldns gkldnfsklnfdklsvn lsdfk vlablvna;fjdo;sagfup934tuip j;a;sjgfd;kasjg;dasgkjdasg;",
                Previous = head
            };
            head.Next = secondNode;

            yield return head;

            head.Random = secondNode;

            yield return head;

            var thirdNode = new ListNode
            {
                Previous = secondNode
            };

            secondNode.Next = thirdNode;

            yield return head;

        }
    }
}