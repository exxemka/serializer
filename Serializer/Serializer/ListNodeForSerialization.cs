﻿namespace Serializer
{
    public readonly struct ListNodeForSerialization
    {
        public ListNodeForSerialization(string data, int? randomNoteIndex)
        {
            RandomNoteIndex = randomNoteIndex;
            Data = data;
        }

        public string Data { get; }
        public int? RandomNoteIndex { get; }
    }
}
