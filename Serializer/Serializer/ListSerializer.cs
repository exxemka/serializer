﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ProtoBuf;

namespace Serializer
{
    /// <summary>
    /// Serializer interface for list based on the ListNode
    /// </summary>
    public interface IListSerializer
    {

        /// <summary>
        /// Serializes all nodes in the list, including topology of the Random links, into stream
        /// </summary>
        Task Serialize(ListNode head, Stream s);

        /// <summary>
        /// Deserializes the list from the stream, returns the head node of the list
        /// </summary>
        /// <exception cref="System.ArgumentException">Thrown when a stream has invalid data</exception>
        Task<ListNode> Deserialize(Stream s);

        /// <summary>
        /// Makes a deep copy of the list, returns the head node of the list 
        /// </summary>
        Task<ListNode> DeepCopy(ListNode head);
    }

    public class ListSerializer : IListSerializer
    {
        public Task Serialize(ListNode head, Stream s)
        {
            if (s is null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            if (!s.CanWrite)
            {
                throw new InvalidOperationException("Unable to write into the stream");
            }

            if (head is null)
            {
                throw new ArgumentNullException(nameof(head));
            }

            var indexByNode = GetIndexByNode(head);

            var currentNode = head;
            while (currentNode != null)
            {
                int? randomNodeIndex = null;
                if (currentNode.Random != null)
                {
                    randomNodeIndex = indexByNode[currentNode.Random];
                }

                var nodeForSerialization = new ListNodeForSerialization(currentNode.Data, randomNodeIndex);
                ProtoBuf.Serializer.SerializeWithLengthPrefix(s, nodeForSerialization, PrefixStyle.Fixed32);

                currentNode = currentNode.Next;
            }

            return Task.CompletedTask;
        }

        public Task<ListNode> Deserialize(Stream s)
        {
            if (s is null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            if (!s.CanRead)
            {
                throw new InvalidOperationException("Unable to read from stream");
            }

            var indexByNode = new Dictionary<int, ListNodeForSerialization>();
            var index = 0;

            while (s.Position < s.Length)
            {
                try
                {
                    var deserializedNode =
                        ProtoBuf.Serializer.DeserializeWithLengthPrefix<ListNodeForSerialization>(s,
                            PrefixStyle.Fixed32);
                    indexByNode.Add(index, deserializedNode);
                    index++;
                }
                catch (Exception)
                {
                    throw new ArgumentException(nameof(s));
                }
            }

            var list = CreateListFromDictionary(indexByNode);

            return Task.FromResult(list);
        }

        public Task<ListNode> DeepCopy(ListNode head)
        {
            if (head is null)
            {
                return Task.FromResult<ListNode>(null);
            }

            using (var stream = new MemoryStream())
            {
                Serialize(head, stream);
                stream.Position = 0;
                var listDeepCopy = Deserialize(stream);

                return listDeepCopy;
            }
        }

        private Dictionary<ListNode, int> GetIndexByNode(ListNode head)
        {
            var currentNode = head;
            var indexByNode = new Dictionary<ListNode, int>();
            var index = 0;

            while (currentNode != null)
            {
                if (!indexByNode.ContainsKey(currentNode))
                {
                    indexByNode.Add(currentNode, index);
                }

                index++;
                currentNode = currentNode.Next;
            }

            return indexByNode;
        }

        private ListNode CreateListFromDictionary(IReadOnlyDictionary<int, ListNodeForSerialization> dictionary)
        {
            if (!dictionary.Keys.Any())
            {
                return null;
            }

            var nodesByIndex = dictionary.ToDictionary(item => item.Key, item => new ListNode
            {
                Data = item.Value.Data
            });

            
            var head = nodesByIndex[0];
            if (dictionary[0].RandomNoteIndex.HasValue)
            {
                head.Random = nodesByIndex[dictionary[0].RandomNoteIndex.Value];
            }
            var previousNode = head;
            var index = 1;

            while (dictionary.ContainsKey(index))
            {
                var currentNode = nodesByIndex[index];

                var currentItem = dictionary[index];
                if (currentItem.RandomNoteIndex.HasValue)
                {
                    currentNode.Random = nodesByIndex[currentItem.RandomNoteIndex.Value];
                }

                currentNode.Previous = previousNode;
                previousNode.Next = currentNode;
                previousNode = currentNode;

                index++;
            }

            return head;
        }
    }
}
